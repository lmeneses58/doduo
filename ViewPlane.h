#ifndef VIEWPLANE_H
#define VIEWPLANE_H

class ViewPlane
{
    public:
        ViewPlane();
        /*S es el tamaño del cuadradito, el tamaño del pixel
          mientras mas pequeño el cuadradito mejor es la calidad de
          imagen, mientras mas grande lo contrario*/
        ViewPlane(int v_hres, int v_vres, double v_s); 
        
    public:
        float s;
        int hres;
        int vres;
};

#endif