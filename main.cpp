#include <vector>
#include <iostream>
#include "Vector3D.h"
#include "Esfera.h"
#include "ViewPlane.h"
#include "Utilitarios.h"
#include "Plano.h"
#include "Triangulo.h"
#include "LuzPuntual.h"
#include "math.h"

using namespace std;

double acotar(double n)
{
    if (n > 1.0)
    {
        return 1.0;
    }
    else
    {
        return n;
    }
}

ColorRGB obtenerColorPixel(const Rayo &r, vector<ObjetoGeometrico *> objetos, LuzPuntual luz)
{
    LuzPuntual luzAmbiente(1.0, 1.0, 1.0, -100, 50, 0);

    ColorRGB color;

    color.r = 0.0;
    color.g = 0.0;
    color.b = 0.0;

    double t;
    double tmin = 2000000;
    Vector3D n;
    Punto3D q;

    Vector3D h;
    for (int i = 0; i < objetos.size(); i++)
    {
        if (objetos[i]->hayImpacto(r, t, n, q) && t < tmin)
        {

            // color.r = objetos[i]->obtenerColor().r * luz.color.r * std::max(0.0, n * (luz.posicion - q).hat()) ;
            // color.g = objetos[i]->obtenerColor().g * luz.color.g * std::max(0.0, n * (luz.posicion - q).hat());
            // color.b = objetos[i]->obtenerColor().b * luz.color.b * std::max(0.0, n * (luz.posicion - q).hat());

            // h = ((-1)*r.d).hat();
            // color.r = luz.color.r * objetos[i]->obtenerColor().r * std::max(0.0, n * (luz.posicion - q).hat()) + luz.color.r * objetos[i]->obtenerColor().r * pow(max(0.0, n * h), 1000);
            // color.g = luz.color.g * objetos[i]->obtenerColor().g * std::max(0.0, n * (luz.posicion - q).hat()) + luz.color.r * objetos[i]->obtenerColor().r * pow(max(0.0, n * h), 1000);
            // color.b = luz.color.b * objetos[i]->obtenerColor().b * std::max(0.0, n * (luz.posicion - q).hat()) + luz.color.r * objetos[i]->obtenerColor().r * pow(max(0.0, n * h), 1000);

            h = ((-1) * r.d).hat();
            color.r = acotar(0.1 + luz.color.r * objetos[i]->obtenerColor().r * std::max(0.5, n * (luz.posicion - q).hat()) + luz.color.r * objetos[i]->obtenerColor().r * pow(max(0.0, n * h), 100));
            color.g = acotar(0.1 + luz.color.g * objetos[i]->obtenerColor().g * std::max(0.5, n * (luz.posicion - q).hat()) + luz.color.r * objetos[i]->obtenerColor().r * pow(max(0.0, n * h), 100));
            color.b = acotar(0.1 + luz.color.b * objetos[i]->obtenerColor().b * std::max(0.5, n * (luz.posicion - q).hat()) + luz.color.r * objetos[i]->obtenerColor().r * pow(max(0.0, n * h), 100));

            tmin = t;

            // color.r = objetos[i]->obtenerColor().r;
            // color.g = objetos[i]->obtenerColor().g;
            // color.b = objetos[i]->obtenerColor().b;
            // tmin = t;

            // Luz ambiente
            h = ((-1) * r.d).hat();
            color.r = (objetos[i]->obtenerColor().r * luzAmbiente.color.r) + luz.color.r * objetos[i]->obtenerColor().r * std::max(0.0, n * (luz.posicion - q).hat()) + luz.color.r * objetos[i]->obtenerColor().r * pow(max(0.0, n * h), 1000);
            color.g = (objetos[i]->obtenerColor().g * luzAmbiente.color.b) + luz.color.g * objetos[i]->obtenerColor().g * std::max(0.0, n * (luz.posicion - q).hat()) + luz.color.r * objetos[i]->obtenerColor().r * pow(max(0.0, n * h), 1000);
            color.b = (objetos[i]->obtenerColor().b * luzAmbiente.color.b) + luz.color.b * objetos[i]->obtenerColor().b * std::max(0.0, n * (luz.posicion - q).hat()) + luz.color.r * objetos[i]->obtenerColor().r * pow(max(0.0, n * h), 1000);
        }
    }
    return color;
}

int main()
{

    // LUZ---------------------------------------------------------
    LuzPuntual luz(1.0, 1.0, 1.0, -100, 50, 0);

    // CUERPO--------------------------------------------------------------------------------------------
    vector<ObjetoGeometrico *> escena;

    //TRIANGULOS---------------------------------------------

    Punto3D A(-60, 90, -500);
    Punto3D B(-20, -20, -500);
    Punto3D C(-40, -40, -500);
    Triangulo cuello1(B, C, A);
    cuello1.establecerColor(0.78, 0.55, 0.38);

    Punto3D A1(-60, 90, -500);
    Punto3D B1(-40, -40, -500);
    Punto3D C1(-80, 70, -500);
    Triangulo cuello2(B1, C1, A1);
    cuello2.establecerColor(0.78, 0.55, 0.38);

    Punto3D A3(60, 90, -500);
    Punto3D B3(20, -20, -500);
    Punto3D C3(40, -40, -500);
    Triangulo cuello3(C3, B3, A3);
    cuello3.establecerColor(0.78, 0.55, 0.38);

    Punto3D A4(60, 90, -500);
    Punto3D B4(40, -40, -500);
    Punto3D C4(80, 70, -500);
    Triangulo cuello4(C4, B4, A4);
    cuello4.establecerColor(0.78, 0.55, 0.38);

    // CABEZA -----------------------------------------------
    Punto3D centroCabeza1(-80.0, 130.0, -500.0);
    double radioCabeza = 70;
    Esfera cabeza1(centroCabeza1, radioCabeza);
    cabeza1.establecerColor(0.82, 0.55, 0.33);

    Punto3D centroCabeza2(80.0, 130.0, -500.0);
    double radioCabeza2 = 70;
    Esfera cabeza2(centroCabeza2, radioCabeza2);
    cabeza2.establecerColor(0.82, 0.55, 0.33);

    // OJOS 1-------------------------------------------
    Punto3D centroOjoD(-60.0, 130, -400);
    double radioOjoD = 10;
    Esfera ojoDer(centroOjoD, radioOjoD);
    ojoDer.establecerColor(0.14, 0.11, 0.08);

    Punto3D centroOjoI(-105.0, 130, -400);
    double radioOjoI = 10;
    Esfera ojoIzq(centroOjoI, radioOjoI);
    ojoIzq.establecerColor(0.14, 0.11, 0.08);

    Punto3D centroPupilaD(-63.0, 135, -300);
    double radioPupilaD = 4;
    Esfera pupilaD(centroPupilaD, radioPupilaD);
    pupilaD.establecerColor(1.0, 1.0, 1.0);

    Punto3D centroPupilaI(-107.0, 135, -300);
    double radioPupilaI = 4;
    Esfera pupilaI(centroPupilaI, radioPupilaI);
    pupilaI.establecerColor(1.0, 1.0, 1.0);

    // OJOS 2-------------------------------------------
    Punto3D centroOjoD2(60.0, 140, -400);
    double radioOjoD2 = 10;
    Esfera ojoDer2(centroOjoD2, radioOjoD2);
    ojoDer2.establecerColor(0.14, 0.11, 0.08);

    Punto3D centroOjoI2(105.0, 140, -400);
    double radioOjoI2 = 10;
    Esfera ojoIzq2(centroOjoI2, radioOjoI2);
    ojoIzq2.establecerColor(0.14, 0.11, 0.08);

    Punto3D centroPupilaD2(63.0, 145, -300);
    double radioPupilaD2 = 4;
    Esfera pupilaD2(centroPupilaD2, radioPupilaD2);
    pupilaD2.establecerColor(1.0, 1.0, 1.0);

    Punto3D centroPupilaI2(108.0, 145, -300);
    double radioPupilaI2 = 4;
    Esfera pupilaI2(centroPupilaI2, radioPupilaI2);
    pupilaI2.establecerColor(1.0, 1.0, 1.0);
    //PICO---------------------------------------------------
    Punto3D pico1(-82.5, 117, -400);
    double radioPicoD = 15;
    Esfera picoD(pico1, radioPicoD);
    picoD.establecerColor(0.57, 0.46, 0.32);

    Punto3D pico2(82.5, 127, -400);
    double radioPicoD2 = 15;
    Esfera picoD2(pico2, radioPicoD2);
    picoD2.establecerColor(0.57, 0.46, 0.32);

    //CUERPO --------------------------------------------------

    Punto3D centroCuerpo(0, -90, -600);
    double radioCuerpo = 100;
    Esfera cuerpo(centroCuerpo, radioCuerpo);
    cuerpo.establecerColor(0.82, 0.55, 0.33);

    //TRIANGULOS---------------------------------------------

    Punto3D a1(-99.69, -80.03, -600);
    Punto3D b1(-99.69, -99.97, -600);
    Punto3D c1(-114.28, -90.25, -600);
    Triangulo T1(a1, b1, c1);
    T1.establecerColor(0.82, 0.55, 0.33);

    Punto3D a2(-95.72, -60.39, -600);
    Punto3D b2(-99.69, -80.03, -600);
    Punto3D c2(-112.05, -67.61, -600);
    Triangulo T2(a2, b2, c2);
    T2.establecerColor(0.82, 0.55, 0.33);

    Punto3D a3(-87.98, -42.06, -600);
    Punto3D b3(-95.72, -60.39, -600);
    Punto3D c3(-104.63, -44.56, -600);
    Triangulo T3(a3, b3, c3);
    T3.establecerColor(0.82, 0.55, 0.33);

    Punto3D a4(-76.26, -25.01, -600);
    Punto3D b4(-87.98, -42.06, -600);
    Punto3D c4(-93.82, -24.12, -600);
    Triangulo T4(a4, b4, c4);
    T4.establecerColor(0.82, 0.55, 0.33);

    Punto3D a5(-61.61, -10.99, -600);
    Punto3D b5(-76.26, -25.01, -600);
    Punto3D c5(-78.09, -6.71, -600);
    Triangulo T5(a5, b5, c5);
    T5.establecerColor(0.82, 0.55, 0.33);

    Punto3D a6(-44.03, 0, -600);
    Punto3D b6(-61.61,-10.99, -600);
    Punto3D c6(-59.37, 7.40, -600);
    Triangulo T6(a6, b6, c6);
    T6.establecerColor(0.82, 0.55, 0.33);

    Punto3D a7(-25.93, 6.78, -600);
    Punto3D b7(-44.03, 0, -600);
    Punto3D c7(-38.83, 16.60, -600);
    Triangulo T7(a7, b7, c7);
    T7.establecerColor(0.82, 0.55, 0.33);

    Punto3D a8(-9.97, 9.69, -600);
    Punto3D b8(-25.93, 6.78, -600);
    Punto3D c8(-20.51, 22.94, -600);
    Triangulo T8(a8, b8, c8);
    T8.establecerColor(0.82, 0.55, 0.33);

    Punto3D a9(9.97, 9.69, -600);
    Punto3D b9(-9.97, 9.69, -600);
    Punto3D c9(0, 26.42, -600);
    Triangulo T9(a9, b9, c9);
    T9.establecerColor(0.82, 0.55, 0.33);

    Punto3D a10(-99.69, -99.97, -600);
    Punto3D b10(-95.90, -119.02, -600);
    Punto3D c10(-113.40, -113.84, -600);
    Triangulo T10(a10, b10, c10);
    T10.establecerColor(0.82, 0.55, 0.33);

    Punto3D a11(-95.90, -119.02, -600);
    Punto3D b11(-87.16, -139.42, -600);
    Punto3D c11(-105.90, -136.28, -600);
    Triangulo T11(a11, b11, c11);
    T11.establecerColor(0.82, 0.55, 0.33);

    Punto3D a12(-87.16, -139.42, -600);
    Punto3D b12(-74.58, -156.90, -600);
    Punto3D c12(-93.52, -158.64, -600);
    Triangulo T12(a12, b12, c12);
    T12.establecerColor(0.82, 0.55, 0.33);

    Punto3D a13(-74.58, -156.90, -600);
    Punto3D b13(-57.34, -172.16, -600);
    Punto3D c13(-77.49, -176.70, -600);
    Triangulo T13(a13, b13, c13);
    T13.establecerColor(0.82, 0.55, 0.33);

    Punto3D a14(-57.34, -172.16, -600);
    Punto3D b14(-37.74, -182.81, -600);
    Punto3D c14(-55.21, -191.86, -600);
    Triangulo T14(a14, b14, c14);
    T14.establecerColor(0.82, 0.55, 0.33);

    Punto3D a15(-37.74, -182.81, -600);
    Punto3D b15(-17.99, -188.65, -600);
    Punto3D c15(-32.12, -202.05, -600);
    Triangulo T15(a15, b15, c15);
    T15.establecerColor(0.82, 0.55, 0.33);

    Punto3D a16(-17.99, -188.65, -600);
    Punto3D b16(0, -190.19, -600);
    Punto3D c16(-9.84, -206.14, -600);
    Triangulo T16(a16, b16, c16);
    T16.establecerColor(0.82, 0.55, 0.33);

    //------OPUESTOS CUERPO

    Punto3D a1o(99.69, -80.03, -600);
    Punto3D b1o(99.69, -99.97, -600);
    Punto3D c1o(114.28, -90.25, -600);
    Triangulo T1op(b1o, a1o, c1o);
    T1op.establecerColor(0.82, 0.55, 0.33);

    Punto3D a2o(95.72, -60.39, -600);
    Punto3D b2o(99.69, -80.03, -600);
    Punto3D c2o(112.05, -67.61, -600);
    Triangulo T2op(b2o, a2o, c2o);
    T2op.establecerColor(0.82, 0.55, 0.33);

    Punto3D a3o(87.98, -42.06, -600);
    Punto3D b3o(95.72, -60.39, -600);
    Punto3D c3o(104.63, -44.56, -600);
    Triangulo T3op(b3o, a3o, c3o);
    T3op.establecerColor(0.82, 0.55, 0.33);

    Punto3D a4o(76.26, -25.01, -600);
    Punto3D b4o(87.98, -42.06, -600);
    Punto3D c4o(93.82, -24.12, -600);
    Triangulo T4op(b4o, a4o, c4o);
    T4op.establecerColor(0.82, 0.55, 0.33);

    Punto3D a5o(61.61, -10.99, -600);
    Punto3D b5o(76.26, -25.01, -600);
    Punto3D c5o(78.09, -6.71, -600);
    Triangulo T5op(b5o, a5o, c5o);
    T5op.establecerColor(0.82, 0.55, 0.33);

    Punto3D a6o(44.03, 0, -600);
    Punto3D b6o(61.61,-10.99, -600);
    Punto3D c6o(59.37, 7.40, -600);
    Triangulo T6op(b6o, a6o, c6o);
    T6op.establecerColor(0.82, 0.55, 0.33);

    Punto3D a7o(25.93, 6.78, -600);
    Punto3D b7o(44.03, 0, -600);
    Punto3D c7o(38.83, 16.60, -600);
    Triangulo T7op(b7o, a7o, c7o);
    T7op.establecerColor(0.82, 0.55, 0.33);

    Punto3D a8o(9.97, 9.69, -600);
    Punto3D b8o(25.93, 6.78, -600);
    Punto3D c8o(20.51, 22.94, -600);
    Triangulo T8op(b8o, a8o, c8o);
    T8op.establecerColor(0.82, 0.55, 0.33);

    Punto3D a10o(99.69, -99.97, -600);
    Punto3D b10o(95.90, -119.02, -600);
    Punto3D c10o(113.40, -113.84, -600);
    Triangulo T10op(b10o, a10o, c10o);
    T10op.establecerColor(0.82, 0.55, 0.33);

    Punto3D a11o(95.90, -119.02, -600);
    Punto3D b11o(87.16, -139.42, -600);
    Punto3D c11o(105.90, -136.28, -600);
    Triangulo T11op(b11o, a11o, c11o);
    T11op.establecerColor(0.82, 0.55, 0.33);

    Punto3D a12o(87.16, -139.42, -600);
    Punto3D b12o(74.58, -156.90, -600);
    Punto3D c12o(93.52, -158.64, -600);
    Triangulo T12op(b12o, a12o, c12o);
    T12op.establecerColor(0.82, 0.55, 0.33);

    Punto3D a13o(74.58, -156.90, -600);
    Punto3D b13o(57.34, -172.16, -600);
    Punto3D c13o(77.49, -176.70, -600);
    Triangulo T13op(b13o, a13o, c13o);
    T13op.establecerColor(0.82, 0.55, 0.33);

    Punto3D a14o(57.34, -172.16, -600);
    Punto3D b14o(37.74, -182.81, -600);
    Punto3D c14o(55.21, -191.86, -600);
    Triangulo T14op(b14o, a14o, c14o);
    T14op.establecerColor(0.82, 0.55, 0.33);

    Punto3D a15o(37.74, -182.81, -600);
    Punto3D b15o(17.99, -188.65, -600);
    Punto3D c15o(32.12, -202.05, -600);
    Triangulo T15op(b15o, a15o, c15o);
    T15op.establecerColor(0.82, 0.55, 0.33);

    Punto3D a16o(17.99, -188.65, -600);
    Punto3D b16o(0, -190.19, -600);
    Punto3D c16o(9.84, -206.14, -600);
    Triangulo T16op(b16o, a16o, c16o);
    T16op.establecerColor(0.82, 0.55, 0.33);

    //TRIANGULOS CABEZA IZQUIERDA ---------------------------------

    Punto3D AC1(-150,130,-600);
    Punto3D BC1(-140,150,-600);
    Punto3D CC1(-160,140,-600);
    Triangulo TC1(BC1,AC1,CC1);
    TC1.establecerColor(0.82,0.55,0.33);

    Punto3D AC2(-140,110,-600);
    Punto3D BC2(-150,130,-600);
    Punto3D CC2(-160,120,-600);
    Triangulo TC2(BC2,AC2,CC2);
    TC2.establecerColor(0.82,0.55,0.33);
    
    //-----

    Punto3D AC3(-143,161,-600);
    Punto3D BC3(-124,175,-600);
    Punto3D CC3(-147,175,-600);
    Triangulo TC3(BC3,AC3,CC3);
    TC3.establecerColor(0.82,0.55,0.33);

    Punto3D AC4(-143,138,-600);
    Punto3D BC4(-143,161,-600);
    Punto3D CC4(-155,157,-600);
    Triangulo TC4(BC4,AC4,CC4);
    TC4.establecerColor(0.82,0.55,0.33);

    //-----

    Punto3D AC5(-143,100,-600); 
    Punto3D BC5(-143,121,-600);
    Punto3D CC5(-156,104,-600);
    Triangulo TC5(BC5,AC5,CC5);
    TC5.establecerColor(0.82,0.55,0.33);

    Punto3D AC6(-124,86,-600);
    Punto3D BC6(-143,100,-600); 
    Punto3D CC6(-148,86,-600);
    Triangulo TC6(BC6,AC6,CC6);
    TC6.establecerColor(0.82,0.55,0.33);
    
    //-----

    Punto3D AC7(-39,74,-600); 
    Punto3D BC7(-29,94,-600);
    Punto3D CC7(-24,72,-600);
    Triangulo TC7(AC7,BC7,CC7);
    TC7.establecerColor(0.82,0.55,0.33);

    Punto3D AC8(-60,70,-600);
    Punto3D BC8(-39,74,-600); 
    Punto3D CC8(-40,60,-600);
    Triangulo TC8(AC8,BC8,CC8);
    TC8.establecerColor(0.82,0.55,0.33);
    
    //-----

    Punto3D AC11(-10,130,-600);
    Punto3D BC11(-20,150,-600);
    Punto3D CC11(-0,140,-600);
    Triangulo TC11(AC11,BC11,CC11);
    TC11.establecerColor(0.82,0.55,0.33);

    Punto3D AC22(-20,110,-600);
    Punto3D BC22(-10,130,-600);
    Punto3D CC22(-0,120,-600);
    Triangulo TC22(AC22,BC22,CC22);
    TC22.establecerColor(0.82,0.55,0.33);

    //-----

    Punto3D AC33(-18,161,-600);
    Punto3D BC33(-36,175,-600);
    Punto3D CC33(-13,175,-600);
    Triangulo TC33(AC33,BC33,CC33);
    TC33.establecerColor(0.82,0.55,0.33);

    Punto3D AC44(-18,138,-600);
    Punto3D BC44(-18,161,-600);
    Punto3D CC44(-3,157,-600);
    Triangulo TC44(AC44,BC44,CC44);
    TC44.establecerColor(0.82,0.55,0.33);

    //-----

    Punto3D AC55(-18,100,-600); 
    Punto3D BC55(-18,121,-600);
    Punto3D CC55(-4,104,-600);
    Triangulo TC55(AC55,BC55,CC55);
    TC55.establecerColor(0.82,0.55,0.33);

    Punto3D AC66(-36,86,-600);
    Punto3D BC66(-18,100,-600); 
    Punto3D CC66(-13,86,-600);
    Triangulo TC66(AC66,BC66,CC66);
    TC66.establecerColor(0.82,0.55,0.33);

    //-----

    Punto3D AC77(-122,74,-600); 
    Punto3D BC77(-131,94,-600);
    Punto3D CC77(-136,72,-600);
    Triangulo TC77(BC77,AC77,CC77);
    TC77.establecerColor(0.82,0.55,0.33);

    Punto3D AC88(-100,70,-600);
    Punto3D BC88(-122,74,-600); 
    Punto3D CC88(-120,60,-600);
    Triangulo TC88(BC88,AC88,CC88);
    TC88.establecerColor(0.82,0.55,0.33);

    //-----

    Punto3D AB1(-93,63,-600); 
    Punto3D BB1(-110,75,-600);
    Punto3D CB1(-104,53,-600);
    Triangulo TB1(BB1,AB1,CB1);
    TB1.establecerColor(0.82,0.55,0.33);

    Punto3D AB2(-71,68,-600); 
    Punto3D BB2(-93,63,-600);
    Punto3D CB2(-84,49,-600);
    Triangulo TB2(BB2,AB2,CB2);
    TB2.establecerColor(0.82,0.55,0.33);

    //-----

    Punto3D AB3(-122,186,-600); 
    Punto3D BB3(-100,190,-600);
    Punto3D CB3(-120,200,-600);
    Triangulo TB3(BB3,AB3,CB3);
    TB3.establecerColor(0.82,0.55,0.33);

    Punto3D AB4(-131,167,-600); 
    Punto3D BB4(-122,186,-600); 
    Punto3D CB4(-136,188,-600);
    Triangulo TB4(BB4,AB4,CB4);
    TB4.establecerColor(0.82,0.55,0.33);

    //-----

    Punto3D AB5(-92,199,-600); 
    Punto3D BB5(-70,192,-600);
    Punto3D CB5(-85,211,-600);
    Triangulo TB5(BB5,AB5,CB5);
    TB5.establecerColor(0.82,0.55,0.33);

    Punto3D AB6(-110,186,-600); 
    Punto3D BB6(-92,199,-600); 
    Punto3D CB6(-103,207,-600);
    Triangulo TB6(BB6,AB6,CB6);
    TB6.establecerColor(0.82,0.55,0.33);

    //-----

    Punto3D AB7(-62,197,-600); 
    Punto3D BB7(-45,182,-600);
    Punto3D CB7(-49,204,-600);
    Triangulo TB7(BB7,AB7,CB7);
    TB7.establecerColor(0.82,0.55,0.33);

    Punto3D AB8(-84,193,-600); 
    Punto3D BB8(-62,197,-600);  
    Punto3D CB8(-68,210,-600);
    Triangulo TB8(BB8,AB8,CB8);
    TB8.establecerColor(0.82,0.55,0.33);
    
    // -----
    Punto3D AB9(-34,183,-600); 
    Punto3D BB9(-26,162,-600);
    Punto3D CB9(-21,185,-600);
    Triangulo TB9(BB9,AB9,CB9);
    TB9.establecerColor(0.82,0.55,0.33);

    Punto3D AB0(-56,188,-600); 
    Punto3D BB0(-34,183,-600);
    Punto3D CB0(-35,196,-600);
    Triangulo TB0(BB0,AB0,CB0);
    TB0.establecerColor(0.82,0.55,0.33);
    
    //TRIANGULOS CABEZA DERECHA ---------------------------------

    Punto3D AD1(150,130,-600);
    Punto3D BD1(140,150,-600);
    Punto3D CD1(160,140,-600);
    Triangulo TD1(AD1,BD1,CD1);
    TD1.establecerColor(0.82,0.55,0.33);

    Punto3D AD2(140,110,-600);
    Punto3D BD2(150,130,-600);
    Punto3D CD2(160,120,-600);
    Triangulo TD2(AD2,BD2,CD2);
    TD2.establecerColor(0.82,0.55,0.33);

    //-----

    Punto3D AD3(143,161,-600);
    Punto3D BD3(124,175,-600);
    Punto3D CD3(147,175,-600);
    Triangulo TD3(AD3,BD3,CD3);
    TD3.establecerColor(0.82,0.55,0.33);

    Punto3D AD4(143,138,-600);
    Punto3D BD4(143,161,-600);
    Punto3D CD4(155,157,-600);
    Triangulo TD4(AD4,BD4,CD4);
    TD4.establecerColor(0.82,0.55,0.33);

    //-----

    Punto3D AD5(143,100,-600); 
    Punto3D BD5(143,121,-600);
    Punto3D CD5(156,104,-600);
    Triangulo TD5(AD5,BD5,CD5);
    TD5.establecerColor(0.82,0.55,0.33);

    Punto3D AD6(124,86,-600);
    Punto3D BD6(143,100,-600); 
    Punto3D CD6(148,86,-600);
    Triangulo TD6(AD6,BD6,CD6);
    TD6.establecerColor(0.82,0.55,0.33);
    
    //-----

    Punto3D AD7(39,74,-600); 
    Punto3D BD7(29,94,-600);
    Punto3D CD7(24,72,-600);
    Triangulo TD7(BD7,AD7,CD7);
    TD7.establecerColor(0.82,0.55,0.33);

    Punto3D AD8(60,70,-600);
    Punto3D BD8(39,74,-600); 
    Punto3D CD8(40,60,-600);
    Triangulo TD8(BD8,AD8,CD8);
    TD8.establecerColor(0.82,0.55,0.33);
    
    //-----

    Punto3D AD11(10,130,-600);
    Punto3D BD11(20,150,-600);
    Punto3D CD11(0,140,-600);
    Triangulo TD11(BD11,AD11,CD11);
    TD11.establecerColor(0.82,0.55,0.33);

    Punto3D AD22(20,110,-600);
    Punto3D BD22(10,130,-600);
    Punto3D CD22(0,120,-600);
    Triangulo TD22(BD22,AD22,CD22);
    TD22.establecerColor(0.82,0.55,0.33);

    // //-----

    Punto3D AD33(18,161,-600);
    Punto3D BD33(36,175,-600);
    Punto3D CD33(13,175,-600);
    Triangulo TD33(BD33,AD33,CD33);
    TD33.establecerColor(0.82,0.55,0.33);

    Punto3D AD44(18,138,-600);
    Punto3D BD44(18,161,-600);
    Punto3D CD44(3,157,-600);
    Triangulo TD44(BD44,AD44,CD44);
    TD44.establecerColor(0.82,0.55,0.33);

    //-----

    Punto3D AD55(18,100,-600); 
    Punto3D BD55(18,121,-600);
    Punto3D CD55(4,104,-600);
    Triangulo TD55(BD55,AD55,CD55);
    TD55.establecerColor(0.82,0.55,0.33);

    Punto3D AD66(36,86,-600);
    Punto3D BD66(18,100,-600); 
    Punto3D CD66(13,86,-600);
    Triangulo TD66(BD66,AD66,CD66);
    TD66.establecerColor(0.82,0.55,0.33);

    //-----

    Punto3D AD77(122,74,-600); 
    Punto3D BD77(131,94,-600);
    Punto3D CD77(136,72,-600);
    Triangulo TD77(AD77,BD77,CD77);
    TD77.establecerColor(0.82,0.55,0.33);

    Punto3D AD88(100,70,-600);
    Punto3D BD88(122,74,-600); 
    Punto3D CD88(120,60,-600);
    Triangulo TD88(AD88,BD88,CD88);
    TD88.establecerColor(0.82,0.55,0.33);

    //-----

    Punto3D ADD1(93,63,-600); 
    Punto3D BDD1(110,75,-600);
    Punto3D CDD1(104,53,-600);
    Triangulo TDD1(ADD1,BDD1,CDD1);
    TDD1.establecerColor(0.82,0.55,0.33);

    Punto3D ADD2(71,68,-600); 
    Punto3D BDD2(93,63,-600);
    Punto3D CDD2(84,49,-600);
    Triangulo TDD2(ADD2,BDD2,CDD2);
    TDD2.establecerColor(0.82,0.55,0.33);

    // //-----

    Punto3D ADD3(122,186,-600); 
    Punto3D BDD3(100,190,-600);
    Punto3D CDD3(120,200,-600);
    Triangulo TDD3(ADD3,BDD3,CDD3);
    TDD3.establecerColor(0.82,0.55,0.33);

    Punto3D ADD4(131,167,-600); 
    Punto3D BDD4(122,186,-600); 
    Punto3D CDD4(136,188,-600);
    Triangulo TDD4(ADD4,BDD4,CDD4);
    TDD4.establecerColor(0.82,0.55,0.33);

    // //-----

    Punto3D ADD5(92,199,-600); 
    Punto3D BDD5(70,192,-600);
    Punto3D CDD5(85,211,-600);
    Triangulo TDD5(ADD5,BDD5,CDD5);
    TDD5.establecerColor(0.82,0.55,0.33);

    Punto3D ADD6(110,186,-600); 
    Punto3D BDD6(92,199,-600); 
    Punto3D CDD6(103,207,-600);
    Triangulo TDD6(ADD6,BDD6,CDD6);
    TDD6.establecerColor(0.82,0.55,0.33);

    // //-----

    Punto3D ADD7(62,197,-600); 
    Punto3D BDD7(45,182,-600);
    Punto3D CDD7(49,204,-600);
    Triangulo TDD7(ADD7,BDD7,CDD7);
    TDD7.establecerColor(0.82,0.55,0.33);

    Punto3D ADD8(84,193,-600); 
    Punto3D BDD8(62,197,-600);  
    Punto3D CDD8(68,210,-600);
    Triangulo TDD8(ADD8,BDD8,CDD8);
    TDD8.establecerColor(0.82,0.55,0.33);
    
    // -----
    Punto3D ADD9(34,183,-600); 
    Punto3D BDD9(26,162,-600);
    Punto3D CDD9(21,185,-600);
    Triangulo TDD9(ADD9,BDD9,CDD9);
    TDD9.establecerColor(0.82,0.55,0.33);

    Punto3D ADD0(56,188,-600); 
    Punto3D BDD0(34,183,-600);
    Punto3D CDD0(35,196,-600);
    Triangulo TDD0(ADD0,BDD0,CDD0);
    TDD0.establecerColor(0.82,0.55,0.33);






    ColorRGB color_pixel;

    escena.push_back(&cabeza1);
    escena.push_back(&cabeza2);
    escena.push_back(&ojoDer);
    escena.push_back(&ojoIzq);
    escena.push_back(&pupilaD);
    escena.push_back(&pupilaI);
    escena.push_back(&picoD);
    escena.push_back(&ojoDer2);
    escena.push_back(&ojoIzq2);
    escena.push_back(&pupilaI2);
    escena.push_back(&pupilaD2);
    escena.push_back(&picoD2);
    escena.push_back(&cuerpo);
    escena.push_back(&cuello1);
    escena.push_back(&cuello2);
    escena.push_back(&cuello3);
    escena.push_back(&cuello4);
    escena.push_back(&T1);
    escena.push_back(&T2);
    escena.push_back(&T3);
    escena.push_back(&T4);
    escena.push_back(&T5);
    escena.push_back(&T6);
    escena.push_back(&T7);
    escena.push_back(&T8);
    escena.push_back(&T9);
    escena.push_back(&T10);
    escena.push_back(&T11);
    escena.push_back(&T12);
    escena.push_back(&T13);
    escena.push_back(&T14);
    escena.push_back(&T15);
    escena.push_back(&T16);
    escena.push_back(&T1op);
    escena.push_back(&T2op);
    escena.push_back(&T3op);
    escena.push_back(&T4op);
    escena.push_back(&T5op);
    escena.push_back(&T6op);
    escena.push_back(&T7op);
    escena.push_back(&T8op);
    escena.push_back(&T10op);
    escena.push_back(&T11op);
    escena.push_back(&T12op);
    escena.push_back(&T13op);
    escena.push_back(&T14op);
    escena.push_back(&T15op);
    escena.push_back(&T16op);
    escena.push_back(&TC1);
    escena.push_back(&TC2);
    escena.push_back(&TC3);
    escena.push_back(&TC4);
    escena.push_back(&TC5);
    escena.push_back(&TC6);
    escena.push_back(&TC7);
    escena.push_back(&TC8);
    escena.push_back(&TC11);
    escena.push_back(&TC22);
    escena.push_back(&TC33);
    escena.push_back(&TC44);
    escena.push_back(&TC55);
    escena.push_back(&TC66);
    escena.push_back(&TC77);
    escena.push_back(&TC88);
    escena.push_back(&TB1);
    escena.push_back(&TB2);
    escena.push_back(&TB3);
    escena.push_back(&TB4);
    escena.push_back(&TB5);
    escena.push_back(&TB6);
    escena.push_back(&TB7);
    escena.push_back(&TB8);
    escena.push_back(&TB9);
    escena.push_back(&TB0);
    escena.push_back(&TD1);
    escena.push_back(&TD2);
    escena.push_back(&TD3);
    escena.push_back(&TD4);
    escena.push_back(&TD5);
    escena.push_back(&TD6);
    escena.push_back(&TD7);
    escena.push_back(&TD8);
    escena.push_back(&TD11);
    escena.push_back(&TD22);
    escena.push_back(&TD33);
    escena.push_back(&TD44);
    escena.push_back(&TD55);
    escena.push_back(&TD66);
    escena.push_back(&TD77);
    escena.push_back(&TD88);
    escena.push_back(&TDD1);
    escena.push_back(&TDD2);
    escena.push_back(&TDD3);
    escena.push_back(&TDD4);
    escena.push_back(&TDD5);
    escena.push_back(&TDD6);
    escena.push_back(&TDD7);
    escena.push_back(&TDD8);
    escena.push_back(&TDD9);
    escena.push_back(&TDD0);

    // VIEWPLANE
    int hres = 800;
    int vres = 600;
    double s = 1.0; //Tamaño del pixel
    ViewPlane vp(hres, vres, s);

    // UTILITARIO PARA GUARDAR IMAGEN -------------------------------------------------------------------
    int dpi = 72;
    int width = vp.hres;
    int height = vp.vres;
    int n = width * height;
    ColorRGB *pixeles = new ColorRGB[n];

    // ALGORITMO
    for (int fil = 0; fil < vp.vres; fil++)
    {
        for (int col = 0; col < vp.hres; col++)
        {
            // Disparar un rayo
            Vector3D direccion(0.0, 0.0, -1.0);
            double x_o = vp.s * (col - vp.hres / 2 + 0.5);
            double y_o = vp.s * (fil - vp.vres / 2 + 0.5);
            double z_o = 100;
            Punto3D origen(x_o, y_o, z_o);
            Rayo rayo(origen, direccion);

            // color_pixel = obtenerColorPixel(rayo, escena, luz);

            pixeles[fil * width + col].r = (obtenerColorPixel(rayo, escena, luz).r);
            pixeles[fil * width + col].g = (obtenerColorPixel(rayo, escena, luz).g);
            pixeles[fil * width + col].b = obtenerColorPixel(rayo, escena, luz).b;
        }
    }

    savebmp("imagen.bmp", width, height, dpi, pixeles);
    return 0;
}