#ifndef COLOR_RBG
#define COLOR_RBG

struct ColorRGB
{
    double r;
    double g;
    double b;
};
#endif